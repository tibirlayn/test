import java.util.*;

public class TestArray {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        Random random = new Random();
        int max = 10;
        for (int i = 0; i < 10; i++) {
            list.add(random.nextInt(max));
        }
        System.out.println("Begin");
        for (int x: list) {
            System.out.print(x + " ");
        }
        System.out.println("");
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i) == list.get(j)) {
                    list.remove(j);
                    j--;
                }
            }
        }
        System.out.println("End");
        for (int x: list) {
            System.out.print(x + " ");
        }
    }
}
