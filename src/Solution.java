import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
            BufferedReader read = new BufferedReader( new InputStreamReader(System.in));
            String word = read.readLine();

            String wordFin = "";

            for (String cell : word.split(" ")){
                if (cell.length() > 0){
                    wordFin += cell.substring(0,1).toUpperCase();
                    wordFin += cell.substring(1);
                }
                wordFin += " ";

            }
            wordFin = wordFin.substring(0, wordFin.length() - 1);
            System.out.println(wordFin);
        }
}

